#include <mach/mach.h>
#include <unistd.h>

extern kern_return_t bootstrap_check_in(mach_port_t,const char*,mach_port_t*);

int main() {
  mach_port_t port;
  if(task_get_bootstrap_port(mach_task_self(),&port)!=KERN_SUCCESS
   || bootstrap_check_in(port,"private.launchctl.sshd",&port)!=KERN_SUCCESS){return -1;}
  mach_msg_header_t msg;
  mach_msg(&msg,MACH_RCV_MSG,0,sizeof(mach_msg_header_t),port,0,MACH_PORT_NULL);
  return execl("/bin/launchctl","/bin/launchctl",msg.msgh_id?"load":"unload","-w",
   "/Library/LaunchDaemons/com.openssh.sshd.plist",NULL);
}
