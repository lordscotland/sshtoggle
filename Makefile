ARCHS = armv7

include theos/makefiles/common.mk

LIBRARY_NAME = Toggle
Toggle_FILES = Toggle.m
Toggle_FRAMEWORKS = UIKit
Toggle_INSTALL_PATH = /var/mobile/Library/NCSettings/Toggles/SSH

include $(THEOS_MAKE_PATH)/library.mk

TOOL_NAME = launchctl_sshd

launchctl_sshd_FILES = main.c
launchctl_sshd_INSTALL_PATH = /usr/libexec

include $(THEOS_MAKE_PATH)/tool.mk
