#include <launch.h>
#include <mach/mach.h>

extern kern_return_t bootstrap_look_up(mach_port_t,const char*,mach_port_t*);

BOOL isEnabled() {
  BOOL enabled=NO;
  launch_data_t msg=launch_data_new_string(LAUNCH_KEY_GETJOBS),data=launch_msg(msg);
  launch_data_free(msg);
  if(data){
    if(launch_data_get_type(data)==LAUNCH_DATA_DICTIONARY &&
     launch_data_dict_lookup(data,"com.openssh.sshd")!=NULL){enabled=YES;}
    launch_data_free(data);
  }
  return enabled;
}
void setState(BOOL enable) {
  static mach_port_t port=MACH_PORT_NULL;
  if(port==MACH_PORT_NULL
   && (task_get_bootstrap_port(mach_task_self(),&port)!=KERN_SUCCESS
   || bootstrap_look_up(port,"private.launchctl.sshd",&port)!=KERN_SUCCESS)){
    port=MACH_PORT_NULL;
  }
  else {
    mach_msg(&(mach_msg_header_t){
     .msgh_id=enable,.msgh_bits=MACH_MSGH_BITS(MACH_MSG_TYPE_COPY_SEND,0),
     .msgh_remote_port=port},MACH_SEND_MSG,sizeof(mach_msg_header_t),
     0,MACH_PORT_NULL,MACH_MSG_TIMEOUT_NONE,MACH_PORT_NULL);
  }
}
NSString* localizedName() {
  return @"SSH";
}

// Overwrite respring command
#include <objc/message.h>
static void $_respring() {
  objc_msgSend([UIApplication sharedApplication],
   @selector(terminateWithSuccess));
}
static __attribute__((constructor)) void _init() {
  method_setImplementation(class_getInstanceMethod(
   objc_getClass("NCSOptionsView"),@selector(power_respring)),
   (IMP)$_respring);
}
